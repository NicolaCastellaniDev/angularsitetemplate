export interface MenuItem {
  url: string;
  label: string;
  disabled?: boolean;
}
