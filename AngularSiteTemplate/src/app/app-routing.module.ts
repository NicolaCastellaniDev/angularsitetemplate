import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { environment } from 'src/environments/environment';

@NgModule({
  imports: [RouterModule.forRoot(environment.routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
