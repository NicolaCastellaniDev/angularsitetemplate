import { Component, OnInit } from '@angular/core';
import {
  NgxGalleryOptions,
  NgxGalleryImage,
  NgxGalleryAnimation
} from 'ngx-gallery';

@Component({
  selector: 'app-image-gallery',
  templateUrl: './image-gallery.component.html',
  styleUrls: ['./image-gallery.component.scss']
})
export class ImageGalleryComponent implements OnInit {
  galleryOptions: NgxGalleryOptions[];
  galleryImages: NgxGalleryImage[];
  constructor() {}

  ngOnInit() {
    this.galleryOptions = [
      {
        width: '50vw',
        height: '75vh',
        thumbnailsColumns: 3,
        imageAnimation: NgxGalleryAnimation.Fade,
        imageAutoPlay: false,
        imageAutoPlayPauseOnHover: true,
        previewAutoPlay: false,
        previewAutoPlayPauseOnHover: true,
        previewCloseOnClick: true,
        imageArrows: true,
        previewCloseOnEsc: false,
        imageDescription: true,
        previewZoom: true,
        previewRotate: true
      },
      // max-width 800
      {
        breakpoint: 1400,
        width: '100%',
        height: '75vh',
        imagePercent: 80,
        thumbnailsPercent: 20,
        thumbnailsMargin: 20,
        thumbnailMargin: 20
      },
      // max-width 400
      {
        breakpoint: 400,
        preview: true
      }
    ];

    this.galleryImages = [
      {
        small: '../../../assets/images/home_3.jpg',
        medium: '../../../assets/images/home_3.jpg',
        big: '../../../assets/images/home_3.jpg',
        description: 'Lorem ipsum dolor sit amet!'
      },
      {
        small: '../../../assets/images/home_2.jpg',
        medium: '../../../assets/images/home_2.jpg',
        big: '../../../assets/images/home_2.jpg',
        description: 'Lorem ipsum dolor sit amet!'
      },
      {
        small: '../../../assets/images/Evento.jpeg',
        medium: '../../../assets/images/Evento.jpeg',
        big: '../../../assets/images/Evento.jpeg',
        description: 'Lorem ipsum dolor sit amet!'
      }
    ];
  }
}
