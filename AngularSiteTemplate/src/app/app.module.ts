import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomePageComponent } from './home-page/home-page.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { HeaderComponent } from './header/header.component';

import { NgxGalleryModule } from 'ngx-gallery';
import { ImageGalleryComponent } from './components/image-gallery/image-gallery.component';

import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { NgxUiLoaderModule, POSITION, SPINNER, PB_DIRECTION } from 'ngx-ui-loader';
import { ChiSiamoComponent } from './pages/chi-siamo/chi-siamo.component';
import { DocumentiComponent } from './pages/documenti/documenti.component';


const loaderConfig = {
  'bgsColor': '#2d98da',
  'bgsOpacity': 1,
  'bgsPosition': POSITION.centerCenter,
  'bgsSize': 60,
  'bgsType': SPINNER.threeStrings,
  'blur': 15,
  'fgsColor': '#2d98da',
  'fgsPosition': POSITION.centerCenter,
  'fgsSize': 50,
  'fgsType': SPINNER.pulse,
  'gap': 24,
  'masterLoaderId': 'master',
  'overlayBorderRadius': '0',
  'overlayColor': 'rgba(0,0,0,1)',
  'pbColor': '#2d98da',
  'pbDirection': PB_DIRECTION.leftToRight,
  'pbThickness': 3,
  'hasProgressBar': true,
  'text': 'Caricamento in corso',
  'textColor': '#FFFFFF',
  'textPosition': POSITION.centerCenter,
  'threshold': 500
};

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    NotFoundComponent,
    HeaderComponent,
    ImageGalleryComponent,
    ChiSiamoComponent,
    DocumentiComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgxGalleryModule,
    AngularFontAwesomeModule,
    NgxUiLoaderModule.forRoot(loaderConfig)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
