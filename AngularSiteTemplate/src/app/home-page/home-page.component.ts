import { Component, OnInit } from '@angular/core';
import { AngularFontAwesomeComponent } from 'angular-font-awesome';
@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {
  sponsors = [
    {
      image: `url('../../assets/images/ALBERTI.jpg')`,
      alt: 'ALBERTI ACCIAIO INOX'
    },
    {
      image: `url('../../assets/images/ferrari_BK.png')`,
      alt: 'FERRARI BK'
    },
    {
      image: `url('../../assets/images/gelateria_ciao.png')`,
      alt: 'GELATERIA CIAO'
    }
  ];
  currentSponsors = this.sponsors;
  events = [
    {
      label: 'Il Melodramma italiano del diciannovesimo secolo',
      date: '01/01/2018',
      url: '#',
      expired: true,
      image: `url('../../assets/images/riparo-tagliente-1995.900.jpg')`,
      description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit.
      Donec sed lacinia felis, a fermentum dolor. Cras vitae dui enim.
      Nam ut nibh sodales, pellentesque dui a, aliquam ipsum.
      Cras ornare, nisi in auctor aliquet, leo nunc convallis sapien,
      nec sodales nisi sem eget urna. Nullam ac mi a elit placerat euismod a ultrices tortor.
      Etiam pellentesque odio enim, consequat varius turpis laoreet eget. Sed ac justo id sem
      aliquet bibendum non sit amet ligula. In viverra tempus sagittis.
      Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
      Aenean consequat neque nunc, quis feugiat velit tempus non. Nunc sagittis tortor non tincidunt sollicitudin.
      Quisque placerat turpis ut libero interdum viverra. Donec id congue erat. Nunc efficitur metus a neque lobortis,
      non ultrices massa vulputate. Vivamus accumsan turpis cursus convallis mattis.`
    },
    {
      label: 'Il Melodramma italiano del XIX secolo',
      date: '01/15/2018',
      url: '#',
      expired: true,
      image: `url('../../assets/images/riparo-tagliente-1995.900.jpg')`,
      description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit.
      Donec sed lacinia felis, a fermentum dolor. Cras vitae dui enim.
      Nam ut nibh sodales, pellentesque dui a, aliquam ipsum.
      Cras ornare, nisi in auctor aliquet, leo nunc convallis sapien,
      nec sodales nisi sem eget urna. Nullam ac mi a elit placerat euismod a ultrices tortor.
      Etiam pellentesque odio enim, consequat varius turpis laoreet eget. Sed ac justo id sem
      aliquet bibendum non sit amet ligula. In viverra tempus sagittis.
      Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
      Aenean consequat neque nunc, quis feugiat velit tempus non. Nunc sagittis tortor non tincidunt sollicitudin.
      Quisque placerat turpis ut libero interdum viverra. Donec id congue erat. Nunc efficitur metus a neque lobortis,
      non ultrices massa vulputate. Vivamus accumsan turpis cursus convallis mattis.`
    },
    {
      label: 'Lezione e uscita guidata al sito Riparo Tagliente di Stallavena',
      date: '02/28/2030',
      url: '#',
      isNext: true,
      image: `url('../../assets/images/riparo-tagliente-1995.900.jpg')`,
      description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit.
      Donec sed lacinia felis, a fermentum dolor. Cras vitae dui enim.
      Nam ut nibh sodales, pellentesque dui a, aliquam ipsum.
      Cras ornare, nisi in auctor aliquet, leo nunc convallis sapien,
      nec sodales nisi sem eget urna. Nullam ac mi a elit placerat euismod a ultrices tortor.
      Etiam pellentesque odio enim, consequat varius turpis laoreet eget. Sed ac justo id sem
      aliquet bibendum non sit amet ligula. In viverra tempus sagittis.
      Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
      Aenean consequat neque nunc, quis feugiat velit tempus non. Nunc sagittis tortor non tincidunt sollicitudin.
      Quisque placerat turpis ut libero interdum viverra. Donec id congue erat. Nunc efficitur metus a neque lobortis,
      non ultrices massa vulputate. Vivamus accumsan turpis cursus convallis mattis.`
    },
    {
      label: 'Le più belle arie d\'opera del 700',
      date: '02/28/2030',
      url: '#',
      image: `url('../../assets/images/riparo-tagliente-1995.900.jpg')`,
      description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit.
      Donec sed lacinia felis, a fermentum dolor. Cras vitae dui enim.
      Nam ut nibh sodales, pellentesque dui a, aliquam ipsum.
      Cras ornare, nisi in auctor aliquet, leo nunc convallis sapien,
      nec sodales nisi sem eget urna. Nullam ac mi a elit placerat euismod a ultrices tortor.
      Etiam pellentesque odio enim, consequat varius turpis laoreet eget. Sed ac justo id sem
      aliquet bibendum non sit amet ligula. In viverra tempus sagittis.
      Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
      Aenean consequat neque nunc, quis feugiat velit tempus non. Nunc sagittis tortor non tincidunt sollicitudin.
      Quisque placerat turpis ut libero interdum viverra. Donec id congue erat. Nunc efficitur metus a neque lobortis,
      non ultrices massa vulputate. Vivamus accumsan turpis cursus convallis mattis.`
    },
    {
      label: 'Le più belle arie d\'opera del 700',
      date: '02/28/2030',
      url: '#',
      image: `url('../../assets/images/riparo-tagliente-1995.900.jpg')`,
      description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit.
      Donec sed lacinia felis, a fermentum dolor. Cras vitae dui enim.
      Nam ut nibh sodales, pellentesque dui a, aliquam ipsum.
      Cras ornare, nisi in auctor aliquet, leo nunc convallis sapien,
      nec sodales nisi sem eget urna. Nullam ac mi a elit placerat euismod a ultrices tortor.
      Etiam pellentesque odio enim, consequat varius turpis laoreet eget. Sed ac justo id sem
      aliquet bibendum non sit amet ligula. In viverra tempus sagittis.
      Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
      Aenean consequat neque nunc, quis feugiat velit tempus non. Nunc sagittis tortor non tincidunt sollicitudin.
      Quisque placerat turpis ut libero interdum viverra. Donec id congue erat. Nunc efficitur metus a neque lobortis,
      non ultrices massa vulputate. Vivamus accumsan turpis cursus convallis mattis.`
    },
    {
      label: 'Le più belle arie d\'opera del 700',
      date: '02/28/2030',
      url: '#',
      image: `url('../../assets/images/riparo-tagliente-1995.900.jpg')`,
      description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit.
      Donec sed lacinia felis, a fermentum dolor. Cras vitae dui enim.
      Nam ut nibh sodales, pellentesque dui a, aliquam ipsum.
      Cras ornare, nisi in auctor aliquet, leo nunc convallis sapien,
      nec sodales nisi sem eget urna. Nullam ac mi a elit placerat euismod a ultrices tortor.
      Etiam pellentesque odio enim, consequat varius turpis laoreet eget. Sed ac justo id sem
      aliquet bibendum non sit amet ligula. In viverra tempus sagittis.
      Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
      Aenean consequat neque nunc, quis feugiat velit tempus non. Nunc sagittis tortor non tincidunt sollicitudin.
      Quisque placerat turpis ut libero interdum viverra. Donec id congue erat. Nunc efficitur metus a neque lobortis,
      non ultrices massa vulputate. Vivamus accumsan turpis cursus convallis mattis.`
    }
  ];

  tick: any;

  constructor() {}

  ngOnInit() {
    this.tick = setInterval(
      () => {
        this.magicSponsors();
      },
      10000, 0
    )
  }

  magicSponsors() {
    const firstSponsor = this.currentSponsors.shift();
    this.currentSponsors = [...this.currentSponsors, firstSponsor];
  }

  formatDate(date: Date) {
    return date.toLocaleString();
  }

  getExpiredEvents() {
    return this.events.filter((e) => new Date(e.date).setHours(0, 0, 0, 0) < new Date().setHours(0, 0, 0, 0));
  }
  getNextEvents() {
    return this.events.filter((e) => new Date(e.date).setHours(0, 0, 0, 0) >= new Date().setHours(0, 0, 0, 0));
  }
}
