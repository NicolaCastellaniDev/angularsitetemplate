import { Component } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Title } from '@angular/platform-browser';
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
  selector: 'app-root',
  template: `
    <app-header></app-header>
    <div class='main-bg'></div>
    <router-outlet></router-outlet>
    <ngx-ui-loader></ngx-ui-loader>
  `,
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = environment.site_title;
  constructor(title: Title, private ngxService: NgxUiLoaderService) {
    title.setTitle(this.title);
    this.ngxService.start(); // start foreground spinner of the master loader with 'default' taskId
    // Stop the foreground loading after 5s
    setTimeout(() => {
      this.ngxService.stop(); // stop foreground spinner of the master loader with 'default' taskId
      document.body.classList.add('ready');
    }, 1000);
  }
}
