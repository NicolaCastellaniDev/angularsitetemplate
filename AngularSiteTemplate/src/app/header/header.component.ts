import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { MenuItem } from '../shared/interfaces/MenuItem';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  title: string = environment.site_title;
  menuItems: MenuItem[] = environment.menuItems;
  menuActive = false;
  constructor() { }

  ngOnInit() {
  }

  onMenuClick(...args: any) {
    this.menuActive = !this.menuActive;
  }

}
