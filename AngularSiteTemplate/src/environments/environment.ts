import { HomePageComponent } from 'src/app/home-page/home-page.component';
import { NotFoundComponent } from 'src/app/not-found/not-found.component';
import { ChiSiamoComponent } from 'src/app/pages/chi-siamo/chi-siamo.component';
import { DocumentiComponent } from 'src/app/pages/documenti/documenti.component';

// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  site_title: 'Università Del Tempo Libero',
  base_url: 'http://localhost',
  base_port: '4200',
  routes: [
    {
      path: '',
      component: HomePageComponent
    },
    {
      path: 'chi-siamo',
      component: ChiSiamoComponent
    },
    {
      path: 'documenti',
      component: DocumentiComponent
    }
  ],
  menuItems: [
    {
      url: '/chi-siamo',
      label: 'Chi siamo'
    },
    {
      url: '/documenti',
      label: 'Documenti'
    },
    {
      url: '/calendario',
      label: 'Calendario'
    },
    {
      url: '/eventi-pubblici',
      label: 'Eventi pubblici'
    },
    {
      url: '/dove-siamo',
      label: 'Dove siamo',
      disabled: true
    },
  ]
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
